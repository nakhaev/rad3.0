
// outsource dependencies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { filter, throttle } from 'lodash';
import { LinkContainer } from 'react-router-bootstrap';
import React, { Component, PureComponent } from 'react';
import { Row, Col, Collapse, ListGroup, ListGroupItem, Badge, OverlayTrigger, Tooltip } from 'react-bootstrap';

// local dependencies
import { Logo } from '../images';
import { history } from '../store';
import UserMenu from './user-menu';
import * as ROUTES from '../constants/routes';
import { MENU_ITEM_TYPE } from '../constants/spec';

class Layout extends PureComponent {
    render () {
        const { children, expanded, menu } = this.props;
        return (<div id="privateLayout" className="container-fluid">
            <Row>
                <div className="private-header">
                    <Col xs={2}>
                        <Link to={ROUTES.PRIVATE_WELCOME_SCREEN.LINK()}>
                            <Logo height="45" tabIndex={-1} />
                        </Link>
                    </Col>
                    <Col xs={10}>
                        <UserMenu />
                    </Col>
                </div>
            </Row>
            <Row>
                <div className={`private-aside ${ expanded ? 'expanded' : 'collapsed'}`}>
                    <div id="menu">
                        <div className="hide-scroll-bar">
                            <ListGroup componentClass="ul">
                                { menu.map((item, index) => (
                                    <AsideMenuItem {...item} expanded={expanded} key={index}/>
                                )) }
                            </ListGroup>
                        </div>
                    </div>
                    <div id="content">
                        <div className="hide-scroll-bar">
                            { children }
                        </div>
                    </div>
                </div>
            </Row>
        </div>);
    }
}
// Check
Layout.propTypes = {
    menu: PropTypes.array,
    expanded: PropTypes.bool,
    children: PropTypes.element.isRequired,
};
// Def
Layout.defaultProps = {
    menu: [],
    expanded: false,
};
// Export
export default connect(
    state => ({ expanded: state.privateLayout.expanded }),
    null
)(Layout);

/**
 * switcher to make nested menu
 *
 * @private
 */
class AsideMenuItem extends Component {
    tooltipOptions () {
        const { expanded, name, disabled } = this.props;
        return {
            disabled,
            delayHide: 0.2 * 1000,
            placement: expanded ? 'top' : 'right',
            delayShow: (expanded ? 8 : 0.2) * 1000,
            overlay: (<Tooltip id={'tooltip-'}> {name} </Tooltip>)
        };
    }

    render () {
        const { expanded, type, icon, name, link, isActive, disabled } = this.props;
        switch (type) {
            default: return (<ListGroupItem> default </ListGroupItem>);
            case MENU_ITEM_TYPE.MENU: return <CollapsibleMenu {...this.props} tooltipOptions={this.tooltipOptions()}/>;
            case MENU_ITEM_TYPE.ACTION: return <OverlayTrigger {...this.tooltipOptions()}>
                <ListGroupItem disabled={disabled}>
                    { icon ? (<i className={icon} aria-hidden="true"> </i>) : '' }
                    &nbsp;{ expanded ? name : '' }
                </ListGroupItem>
            </OverlayTrigger>;
            case MENU_ITEM_TYPE.LINK: return (<OverlayTrigger {...this.tooltipOptions()}>
                <LinkContainer exact={false} to={link} isActive={isActive}>
                    <ListGroupItem disabled={disabled}>
                        { icon ? (<i className={icon} aria-hidden="true"> </i>) : '' }
                        &nbsp;{ expanded ? name : '' }
                    </ListGroupItem>
                </LinkContainer>
            </OverlayTrigger>);
        }
    }
}
// Check
AsideMenuItem.propTypes = {
    link: PropTypes.string,
    icon: PropTypes.string,
    expanded: PropTypes.bool,
    disabled: PropTypes.bool,
    isActive: PropTypes.func,
    name: PropTypes.string.isRequired,
    type: PropTypes.oneOf(Object.keys(MENU_ITEM_TYPE).map(key => MENU_ITEM_TYPE[key])).isRequired,
};
// Def
AsideMenuItem.defaultProps = {
    link: '',
    icon: '',
    disabled: false,
    expanded: false,
    isActive: void(0),
};


class CollapsibleMenu extends Component {
    constructor (...args) {
        super(...args);
        this.state = { open: false, active: false };
        this.throttledAutoToggle = throttle(this.autoToggle, 600);
    }

    componentDidMount () { this.throttledAutoToggle(); }

    componentDidUpdate () { this.throttledAutoToggle(); }

    handleToggle = () => !this.props.disabled&&this.setState({ open: !this.state.open });

    autoToggle = () => {
        const { list } = this.props;
        let active = false;
        const links = filter(list, { type: MENU_ITEM_TYPE.LINK });
        // NOTE find at least one active link from nested list
        for (const { link, isActive } of links) {
            const reg = new RegExp(link, 'i');
            active = reg.test(history.location.pathname) || (isActive&&isActive(null, history.location));
            if (active) {
                break;
            }
        }
        // NOTE prevent triggering render without needs
        if (active !== this.state.active) {
            this.setState({ active, open: active });
        }
    };

    render () {
        const { list, expanded, tooltipOptions } = this.props;
        const { open, active } = this.state;
        const name = expanded ? this.props.name : '';
        const listClassName = `sub-menu-toggle ${active ? 'active' : ''}`;
        const iconClassName = `fa fa-caret-down fa-lg transition ${open ? '' : 'fa-rotate-90'}`;
        return (<ListGroup componentClass="ul" style={{ margin: 0 }}>
            <OverlayTrigger {...tooltipOptions}>
                <ListGroupItem className={listClassName} onClick={this.handleToggle}>
                    <Badge> <i className={iconClassName} aria-hidden="true"> </i> </Badge>
                    { name }
                </ListGroupItem>
            </OverlayTrigger>
            <Collapse in={open}>
                <div className="collapse-box">
                    { list.map((item, index) => (<AsideMenuItem key={index} {...item} expanded={expanded} />)) }
                </div>
            </Collapse>
        </ListGroup>);
    }
}
// Check
CollapsibleMenu.propTypes = {
    list: PropTypes.array,
    expanded: PropTypes.bool,
    disabled: PropTypes.bool,
    name: PropTypes.string.isRequired,
    tooltipOptions: PropTypes.object.isRequired,
};
// Def
CollapsibleMenu.defaultProps = {
    list: [],
    disabled: false,
    expanded: false,
};
