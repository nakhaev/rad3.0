
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

export class CenteredBox extends PureComponent {
    render () {
        const { children, className, holderClassName, ...attr } = this.props;
        return (<div className="centered-box">
            <div className={className+holderClassName} {...attr}>
                { children }
            </div>
        </div>);
    }
}
// Check
CenteredBox.propTypes = {
    className: PropTypes.string,
    holderClassName: PropTypes.string,
    children: PropTypes.node.isRequired,
};
// Def
CenteredBox.defaultProps = {
    className: 'box-holder ',
    holderClassName: '',
};

export default CenteredBox;
