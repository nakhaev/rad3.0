
// outsource dependencies
import React from 'react';
import { Link } from 'react-router-dom';

// local dependencies
// import SignUpForm from './form';
import { Logo } from '../../images';
import CenteredBox from '../../components/centered-box';
import { FORGOT_PASSWORD, SIGN_IN } from '../../constants/routes';

export default function SignUpPage () {
    return (<CenteredBox style={{ width: '460px' }}>
        <div className="row offset-bottom-8">
            <div className="col-xs-6 col-xs-offset-3">
                <Logo className="img-responsive" />
            </div>
        </div>
        <div className="row offset-bottom-6">
            {/*<h1 className="col-xs-12 text-center text-drama">*/}
            {/*<strong> Request to join To CBC </strong>*/}
            {/*</h1>*/}
            <h1 className="col-xs-12 text-center text-drama">
                Sorry, but this functionality currently unavailable...
            </h1>
        </div>
        <div className="row offset-bottom-4">
            {/*<div className="col-xs-12">*/}
            {/*<SignUpForm />*/}
            {/*</div>*/}
        </div>
        <div className="row offset-bottom-10">
            <div className="col-xs-6">
                <Link to={SIGN_IN.LINK()}> Sign in </Link>
            </div>
            <div className="col-xs-6 text-right">
                <Link to={FORGOT_PASSWORD.LINK()}> Forgot password </Link>
            </div>
        </div>
    </CenteredBox>);
}
