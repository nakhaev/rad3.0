
// outsource dependencies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React, { PureComponent } from 'react';
import { Field, reduxForm } from 'redux-form';

// local dependencies
import { Logo } from '../../images';
import Input from '../../components/input';
import { PUBLIC } from '../../actions/types';
import { SIGN_IN } from '../../constants/routes';
import CenteredBox from '../../components/centered-box';
import ErrorMessage from '../../components/alert-error';

class ForgotPasswordPage extends PureComponent {
    render () {
        const { invalid, handleSubmit, expectAnswer, forgotPassword, errorMessage, clearError } = this.props;
        return (<CenteredBox style={{ width: '460px' }}>
            <div className="row offset-bottom-8">
                <div className="col-xs-6 col-xs-offset-3">
                    <Logo className="img-responsive" />
                </div>
            </div>
            <div className="row">
                <h1 className="col-xs-12 text-center offset-bottom-0 text-drama">
                    <strong> Forgot password ? </strong>
                </h1>
            </div>
            <div className="row offset-bottom-4">
                <h3 className="col-xs-12 text-center">
                    Please enter your <strong>email address</strong>, and we&apos;ll send you a password reset email.
                </h3>
            </div>
            <div className="clearfix">
                <div className="col-xs-12">
                    <form name="forgotPassword" onSubmit={ handleSubmit(forgotPassword) }>
                        <div className="offset-bottom-8">
                            <Field
                                type="mail"
                                name="email"
                                placeholder="Email"
                                component={ Input }
                                disabled={ expectAnswer }
                                className="form-control input-lg"
                            />
                        </div>
                        <button
                            type="submit"
                            disabled={ invalid || expectAnswer }
                            className="btn btn-lg btn-block btn-primary offset-bottom-6"
                        >
                            <span> Reset password </span>
                            { expectAnswer&&(<i className="fa fa-spinner fa-spin fa-fw"> </i>) }
                        </button>
                        <ErrorMessage active title={'Error:'} message={errorMessage} onChange={clearError}/>
                    </form>
                </div>
            </div>
            <div className="clearfix offset-bottom-10">
                <div className="col-xs-6"> {/* Place 1*/} </div>
                <div className="col-xs-6 text-right">
                    <Link to={SIGN_IN.LINK()}> Sign In </Link>
                </div>
            </div>
        </CenteredBox>);
    }
}
// Check
ForgotPasswordPage.propTypes = {
    invalid: PropTypes.bool,
    expectAnswer: PropTypes.bool,
    errorMessage: PropTypes.string,
    clearError: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    forgotPassword: PropTypes.func.isRequired,
};
// Def
ForgotPasswordPage.defaultProps = {
    invalid: false,
    expectAnswer: false,
    errorMessage: null,
};

export default reduxForm({
    form: 'forgotPassword',
    /**
     * @param { Object } values - named properties of input data
     * @param { Object } meta - information about form status
     * @returns { Object } - named errors
     * @function validate
     * @public
     */
    validate: (values, meta) => {
        const errors = {};
        // EMAIL
        if (!values.email) {
            errors.email = 'Email is required';
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = 'Invalid email address';
        }
        return errors;
    },
})(connect(
    // mapStateToProps
    state => ({ ...state.forgotPassword }),
    // mapDispatchToProps
    dispatch => ({
        forgotPassword: ({ email }) => dispatch({ type: PUBLIC.FORGOT_PASSWORD.REQUEST, email }),
        clearError: () => dispatch({ type: PUBLIC.FORGOT_PASSWORD.CLEAR }),
    })
)(ForgotPasswordPage));
