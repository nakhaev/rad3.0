
// outsource dependencies
import React from 'react';
import { Link } from 'react-router-dom';

// local dependencies
import SignInForm from './form';
import { Logo } from '../../images';
import CenteredBox from '../../components/centered-box';
import { FORGOT_PASSWORD, SIGN_UP } from '../../constants/routes';

// configuration


/**
 * Sign In page
 *
 * @public
 */
function SignInPage () {
    return (
        <CenteredBox style={{ width: '460px' }}>
            <div className="row offset-bottom-8">
                <div className="col-xs-6 col-xs-offset-3">
                    <Logo className="img-responsive" />
                </div>
            </div>
            <div className="row offset-bottom-8">
                <h1 className="col-xs-12 text-drama">
                    <strong> Welcome To CBC Backend </strong>
                </h1>
            </div>
            <div className="clearfix">
                <div className="col-xs-12">
                    <SignInForm />
                </div>
            </div>
            <div className="clearfix offset-bottom-10">
                <div className="col-xs-6">
                    <Link to={SIGN_UP.LINK()}> Request to join </Link>
                </div>
                <div className="col-xs-6 text-right">
                    <Link to={FORGOT_PASSWORD.LINK()}> Forgot password </Link>
                </div>
            </div>
        </CenteredBox>
    );
}

export default SignInPage;
